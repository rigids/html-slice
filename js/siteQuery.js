jQuery(document).ready(function() {
	jQuery("ul li:first-child").addClass("first");
	jQuery("ul li:last-child").addClass("last");
	
	jQuery('.timeline-slider').slick({
	  centerMode: false,
	  slidesToShow: 1,
	  infinite: false,
	  variableWidth: true,
	  centerPadding: '60px',
	  prevArrow:'<button type="button" class="slick-prev"><i class="fa fa-angle-left theme1"></i></button>',
	  nextArrow:'<button type="button" class="slick-next"><i class="fa fa-angle-right theme1"></i></button>',
	  responsive: [
		{
		  breakpoint: 768,
		  settings: {
			centerMode: false,
			centerPadding: '20px',
			slidesToShow: 1
		  }
		},
		{
		  breakpoint: 480,
		  settings: {
			centerMode: false,
			centerPadding: '20px',
			slidesToShow: 1
		  }
		}
	  ]
	});
	jQuery('.four-item').slick({
        dots: false,
        arrows: true,
        infinite: true,
        speed: 300,
        slidesToShow: 4,
		swipe:true,
		touchMove:true,  
		autoplay: true,
		autoplaySpeed: 5000, 
        slidesToScroll: 1,
		prevArrow:'<button type="button" class="slick-prev"><i class="fa fa-angle-left theme1"></i></button>',
		nextArrow:'<button type="button" class="slick-next"><i class="fa fa-angle-right theme1"></i></button>',
	  responsive: [
		{
		  breakpoint: 768,
		  settings: {
			centerMode: false,
			centerPadding: '20px',
			slidesToShow: 1
		  }
		},
		{
		  breakpoint: 480,
		  settings: {
			centerMode: false,
			centerPadding: '20px',
			slidesToShow: 1
		  }
		}
	  ]
    });	
	$( "#tabs" ).tabs();
	
	ww = document.body.clientWidth;
    jQuery(window).bind('resize orientationchange', function() {
        ww = document.body.clientWidth;
		if (ww<768) {
			mobileMenuInterface();
		}
    });
    if (ww < 768) {
        mobileMenuInterface();
    }
	jQuery("#addScroll").click(function() {
	jQuery('html, body').animate({
	   scrollTop: jQuery("#hmIntro").offset().top
	  }, 2000);
	});
});
function mobileMenuInterface() {
    jQuery(".menu li > a").unbind('click').bind('click', function(e) {
        if (jQuery(this).attr('href') == '#') {
            e.preventDefault();
            jQuery(this).parent("li").children('.sub-menu').toggleClass("subOpenOnClick");
            jQuery(this).parent("li").toggleClass("MobileClicked");
        }
    });
    jQuery(".down-mcaret").unbind('click').bind('click', function(e) {
        e.preventDefault();
        jQuery(this).parent("li").children('.sub-menu').toggleClass("subOpenOnClick");
        jQuery(this).parent("li").toggleClass("MobileClicked");
    });
	jQuery(".nav-button").unbind('click').bind('click', function(e) {
        e.preventDefault();
        jQuery(".nav-button,.menu ul").toggleClass("open");
        jQuery(this).find('.menuIcon').toggleClass('fa-bars fa-times');
    });

}